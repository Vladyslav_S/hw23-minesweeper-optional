"use strict";

let x = 8;
let y = 8;
let mines = 10;

do {
  x = +prompt("Введите ширину поля. Должна быть больше 7:", x);
} while (x === "" || isNaN(+x) || x < 8);

do {
  y = +prompt("Введите высоту поля. Должна быть больше 7:", y);
} while (y === "" || isNaN(+y) || y < 8);

mines = Math.floor((x * y) / 6);

const wrapper = document.createElement("div");
wrapper.style.cssText = `width: ${
  x * (25 + 2)
}px; display: flex; flex-wrap: wrap; justify-content: center; align-items: center;`;

const newGameBtn = document.createElement("button");
newGameBtn.id = "newGameBtn";
newGameBtn.textContent = "Начать игру заново";

let flags = 0;

const minesAndFlagCount = document.createElement("div");
minesAndFlagCount.id = "minesAndFlagCount";
minesAndFlagCount.textContent = `${flags} / ${mines}`;

const style = document.createElement("style");
style.textContent =
  ".item {width: 25px; height: 25px; border: 1px solid black; background-color: grey; text-align: center;} .flag {background-color: white; background-image: url(./img/flag.svg);}";

document.body.prepend(style);

const arrMines = genMines(mines, x, y);

const item = document.createElement("div");
item.classList = "item";

for (let i = 1; i <= x * y; i++) {
  const itemClone = item.cloneNode();
  itemClone.id = i;
  // itemClone.textContent = i;
  for (let j = 0; j < arrMines.length; j++) {
    if (i === arrMines[j]) {
      itemClone.classList.add("mine");
    }
  }
  wrapper.append(itemClone);
}

wrapper.addEventListener("click", click);
wrapper.addEventListener("contextmenu", function (e) {
  e.preventDefault();
});
wrapper.addEventListener("contextmenu", addFlag, false);
// wrapper.addEventListener("dblclick", function (e) {
//   let checkedNeigbors2 = [];
//   if (
//     e.target.classList.contains("open") &&
//     e.target.textContent ===
//       checkNeigbors(e.target, x, checkedNeigbors2, "flag")
//   ) {
//     console.log(true);
//   }
// });
newGameBtn.addEventListener("click", function () {
  document.location.reload();
});

document.body.append(wrapper);
wrapper.before(minesAndFlagCount);

function click(e) {
  if (!document.getElementById("newGameBtn")) {
    wrapper.before(newGameBtn);
  }
  if (e.target.classList.contains("mine")) {
    const showMines = Array.from(document.getElementsByClassName("mine"));
    showMines.forEach((element) => {
      element.style.backgroundColor = "red";
    });

    alert("You lost!");

    // stop clicker
    wrapper.removeEventListener("click", click);
    wrapper.removeEventListener("contextmenu", addFlag);
  } else {
    e.target.style.backgroundColor = "white";
    e.target.classList.add("open");

    let checkedNeigbors = [];

    e.target.textContent = `${checkNeigbors(e.target, x, checkedNeigbors)}`;

    // check if win
    if (
      Array.from(document.getElementsByClassName("open")).length ===
      x * y - mines
    ) {
      alert("You win!!!!");
      // stop clicker
      wrapper.removeEventListener("click", click);
      wrapper.removeEventListener("contextmenu", addFlag);
    }
  }
}

function addFlag(e) {
  if (!e.target.classList.contains("open")) {
    e.target.classList.toggle("flag");

    flags = Array.from(document.getElementsByClassName("flag")).length;
    minesAndFlagCount.textContent = `${flags} / ${mines}`;
  }
}

function genMines(numMines, x, y) {
  const arrMinesPlaces = [];
  for (let i = 0; i < numMines; i++) {
    let generatePlace = gen(x, y);
    // console.log(generatePlace);
    while (arrMinesPlaces.includes(generatePlace)) {
      generatePlace = gen(x, y);
      // console.log("---", generatePlace);
    }
    arrMinesPlaces.push(generatePlace);
  }
  // console.log(arrMinesPlaces);
  return arrMinesPlaces;
}

function gen(x, y) {
  const res = parseInt(Math.random() * x * y + 1);
  return res;
}

function checkNeigbors(targ, x, checkedNeigbors) {
  let countCloseMines = 0;
  let arrNeigbors = [];

  if (targ.id % x === 0) {
    arrNeigbors = [
      //   `${+targ.id + 1}`,
      `${targ.id - 1}`,
      `${+targ.id + x}`,
      //   `${+targ.id + x + 1}`,
      `${+targ.id + x - 1}`,
      `${targ.id - x}`,
      `${targ.id - x - 1}`,
      //   `${targ.id - x + 1}`,
    ];
  } else if (targ.id % x === 1) {
    arrNeigbors = [
      `${+targ.id + 1}`,
      //   `${targ.id - 1}`,
      `${+targ.id + x}`,
      `${+targ.id + x + 1}`,
      //   `${+targ.id + x - 1}`,
      `${targ.id - x}`,
      //   `${targ.id - x - 1}`,
      `${targ.id - x + 1}`,
    ];
  } else {
    arrNeigbors = [
      `${+targ.id + 1}`,
      `${targ.id - 1}`,
      `${+targ.id + x}`,
      `${+targ.id + x + 1}`,
      `${+targ.id + x - 1}`,
      `${targ.id - x}`,
      `${targ.id - x - 1}`,
      `${targ.id - x + 1}`,
    ];
  }

  // console.log(arrNeigbors);s

  arrNeigbors.forEach((el) => {
    if (document.getElementById(`${el}`)) {
      if (document.getElementById(`${el}`).classList.contains("mine")) {
        countCloseMines++;
      }
    }
  });

  // check neighbors if no mines arround
  if (countCloseMines <= 0 && !checkedNeigbors.includes(targ.id)) {
    countCloseMines = "";
    checkedNeigbors.push(targ.id);

    arrNeigbors.forEach((el) => {
      const newItem = document.getElementById(`${el}`);

      if (newItem !== null) {
        newItem.style.backgroundColor = "white";
        newItem.classList.add("open");
        newItem.textContent = `${checkNeigbors(newItem, x, checkedNeigbors)}`;
      }
    });
  }
  return countCloseMines === 0 ? "" : countCloseMines;
}
